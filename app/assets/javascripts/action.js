videoApp.controller('ActionController', ['$scope','curtime', '$timeout','$window', '$sce', '$location', '$http', function($scope, curtime, $timeout, $window, $sce, $location, $http){


var iconsArray = new Array("glass", "music", "search", "envelope-o", "heart", "star", "star-o", "user", "film", "th-large", "th", "th-list", "check", "remove", "close", "times", "search-plus", "search-minus", "power-off", "signal", "gear", "cog", "trash-o", "home", "file-o", "clock-o", "road", "download", "arrow-circle-o-down", "arrow-circle-o-up", "inbox", "play-circle-o", "rotate-right", "repeat", "refresh", "list-alt", "lock", "flag", "headphones", "volume-off", "volume-down", "volume-up", "qrcode", "barcode", "tag", "tags", "book", "bookmark", "print", "camera", "font", "bold", "italic", "text-height", "text-width", "align-left", "align-center", "align-right", "align-justify", "list", "dedent", "outdent", "indent", "video-camera", "photo", "image", "picture-o", "pencil", "map-marker", "adjust", "tint", "edit", "pencil-square-o", "share-square-o", "check-square-o", "arrows", "step-backward", "fast-backward", "backward", "play", "pause", "stop", "forward", "fast-forward", "step-forward", "eject", "chevron-left", "chevron-right", "plus-circle", "minus-circle", "times-circle", "check-circle", "question-circle", "info-circle", "crosshairs", "times-circle-o", "check-circle-o", "ban", "arrow-left", "arrow-right", "arrow-up", "arrow-down", "mail-forward", "share", "expand", "compress", "plus", "minus", "asterisk", "exclamation-circle", "gift", "leaf", "fire", "eye", "eye-slash", "warning", "exclamation-triangle", "plane", "calendar", "random", "comment", "magnet", "chevron-up", "chevron-down", "retweet", "shopping-cart", "folder", "folder-open", "arrows-v", "arrows-h", "bar-chart-o", "bar-chart", "twitter-square", "facebook-square", "camera-retro", "key", "gears", "cogs", "comments", "thumbs-o-up", "thumbs-o-down", "star-half", "heart-o", "sign-out", "linkedin-square", "thumb-tack", "external-link", "sign-in", "trophy", "github-square", "upload", "lemon-o", "phone", "square-o", "bookmark-o", "phone-square", "twitter", "facebook-f", "facebook", "github", "unlock", "credit-card", "rss", "hdd-o", "bullhorn", "bell", "certificate", "hand-o-right", "hand-o-left", "hand-o-up", "hand-o-down", "arrow-circle-left", "arrow-circle-right", "arrow-circle-up", "arrow-circle-down", "globe", "wrench", "tasks", "filter", "briefcase", "arrows-alt", "group", "users", "chain", "link", "cloud", "flask", "cut", "scissors", "copy", "files-o", "paperclip", "save", "floppy-o", "square", "navicon", "reorder", "bars", "list-ul", "list-ol", "strikethrough", "underline", "table", "magic", "truck", "pinterest", "pinterest-square", "google-plus-square", "google-plus", "money", "caret-down", "caret-up", "caret-left", "caret-right", "columns", "unsorted", "sort", "sort-down", "sort-desc", "sort-up", "sort-asc", "envelope", "linkedin", "rotate-left", "undo", "legal", "gavel", "dashboard", "tachometer", "comment-o", "comments-o", "flash", "bolt", "sitemap", "umbrella", "paste", "clipboard", "lightbulb-o", "exchange", "cloud-download", "cloud-upload", "user-md", "stethoscope", "suitcase", "bell-o", "coffee", "cutlery", "file-text-o", "building-o", "hospital-o", "ambulance", "medkit", "fighter-jet", "beer", "h-square", "plus-square", "angle-double-left", "angle-double-right", "angle-double-up", "angle-double-down", "angle-left", "angle-right", "angle-up", "angle-down", "desktop", "laptop", "tablet", "mobile-phone", "mobile", "circle-o", "quote-left", "quote-right", "spinner", "circle", "mail-reply", "reply", "github-alt", "folder-o", "folder-open-o", "smile-o", "frown-o", "meh-o", "gamepad", "keyboard-o", "flag-o", "flag-checkered", "terminal", "code", "mail-reply-all", "reply-all", "star-half-empty", "star-half-full", "star-half-o", "location-arrow", "crop", "code-fork", "unlink", "chain-broken", "question", "info", "exclamation", "superscript", "subscript", "eraser", "puzzle-piece", "microphone", "microphone-slash", "shield", "calendar-o", "fire-extinguisher", "rocket", "maxcdn", "chevron-circle-left", "chevron-circle-right", "chevron-circle-up", "chevron-circle-down", "html5", "css3", "anchor", "unlock-alt", "bullseye", "ellipsis-h", "ellipsis-v", "rss-square", "play-circle", "ticket", "minus-square", "minus-square-o", "level-up", "level-down", "check-square", "pencil-square", "external-link-square", "share-square", "compass", "toggle-down", "caret-square-o-down", "toggle-up", "caret-square-o-up", "toggle-right", "caret-square-o-right", "euro", "eur", "gbp", "dollar", "usd", "rupee", "inr", "cny", "rmb", "yen", "jpy", "ruble", "rouble", "rub", "won", "krw", "bitcoin", "btc", "file", "file-text", "sort-alpha-asc", "sort-alpha-desc", "sort-amount-asc", "sort-amount-desc", "sort-numeric-asc", "sort-numeric-desc", "thumbs-up", "thumbs-down", "youtube-square", "youtube", "xing", "xing-square", "youtube-play", "dropbox", "stack-overflow", "instagram", "flickr", "adn", "bitbucket", "bitbucket-square", "tumblr", "tumblr-square", "long-arrow-down", "long-arrow-up", "long-arrow-left", "long-arrow-right", "apple", "windows", "android", "linux", "dribbble", "skype", "foursquare", "trello", "female", "male", "gittip", "gratipay", "sun-o", "moon-o", "archive", "bug", "vk", "weibo", "renren", "pagelines", "stack-exchange", "arrow-circle-o-right", "arrow-circle-o-left", "toggle-left", "caret-square-o-left", "dot-circle-o", "wheelchair", "vimeo-square", "turkish-lira", "try", "plus-square-o", "space-shuttle", "slack", "envelope-square", "wordpress", "openid", "institution", "bank", "university", "mortar-board", "graduation-cap", "yahoo", "google", "reddit", "reddit-square", "stumbleupon-circle", "stumbleupon", "delicious", "digg", "pied-piper", "pied-piper-alt", "drupal", "joomla", "language", "fax", "building", "child", "paw", "spoon", "cube", "cubes", "behance", "behance-square", "steam", "steam-square", "recycle", "automobile", "car", "cab", "taxi", "tree", "spotify", "deviantart", "soundcloud", "database", "file-pdf-o", "file-word-o", "file-excel-o", "file-powerpoint-o", "file-photo-o", "file-picture-o", "file-image-o", "file-zip-o", "file-archive-o", "file-sound-o", "file-audio-o", "file-movie-o", "file-video-o", "file-code-o", "vine", "codepen", "jsfiddle", "life-bouy", "life-buoy", "life-saver", "support", "life-ring", "circle-o-notch", "ra", "rebel", "ge", "empire", "git-square", "git", "hacker-news", "tencent-weibo", "qq", "wechat", "weixin", "send", "paper-plane", "send-o", "paper-plane-o", "history", "genderless", "circle-thin", "header", "paragraph", "sliders", "share-alt", "share-alt-square", "bomb", "soccer-ball-o", "futbol-o", "tty", "binoculars", "plug", "slideshare", "twitch", "yelp", "newspaper-o", "wifi", "calculator", "paypal", "google-wallet", "cc-visa", "cc-mastercard", "cc-discover", "cc-amex", "cc-paypal", "cc-stripe", "bell-slash", "bell-slash-o", "trash", "copyright", "at", "eyedropper", "paint-brush", "birthday-cake", "area-chart", "pie-chart", "line-chart", "lastfm", "lastfm-square", "toggle-off", "toggle-on", "bicycle", "bus", "ioxhost", "angellist", "cc", "shekel", "sheqel", "ils", "meanpath", "buysellads", "connectdevelop", "dashcube", "forumbee", "leanpub", "sellsy", "shirtsinbulk", "simplybuilt", "skyatlas", "cart-plus", "cart-arrow-down", "diamond", "ship", "user-secret", "motorcycle", "street-view", "heartbeat", "venus", "mars", "mercury", "transgender", "transgender-alt", "venus-double", "mars-double", "venus-mars", "mars-stroke", "mars-stroke-v", "mars-stroke-h", "neuter", "facebook-official", "pinterest-p", "whatsapp", "server", "user-plus", "user-times", "hotel", "bed", "viacoin", "train", "subway", "medium");

// var todos = [ {'event':'create','filmname': 'aaaaa','title': 'aaaaa' }, {'event':'create','filmname': 'bbbbb','title': 'bbbbb' }];
var todos = [];
var past_todos = [];

$scope.actionUser = false;
   $scope.actions = [
                  { name: 'zatrzymanie się', 'event': 'stop'},
                  { name: 'oglądanie produktu', 'event': 'watch'},
                  { name: 'dotykanie produktu', 'event': 'touching'},
                  { name: 'zakup', 'event': 'purchase'},
                  { name: 'wyjście', 'event': 'exit'}];
  $scope.sextype = [
                  { name: 'mężczyzna', id: 1},
                  { name: 'kobieta', id: 2}];
  $scope.age = [
                  { name: 'młodzież', id: 1},
                  { name: 'młody dorosły', id: 2},
                  { name: 'starszy dorosły', id: 3}];
  $scope.person = [
                  { name: 'sam', id: 1},
                  { name: 'w towarzystwie', id: 2}];
  $scope.trolley = [
                  { name: 'wózek', id: 1},
                  { name: 'koszyk', id: 2},
                  { name: 'własna torba', id: 3},
                  { name: 'brak', id: 2}];

  $scope.todos = todos;
  $scope.past_todos = past_todos;
  $scope.showImageList = true;
  $scope.showImage = true;


  // $http.get('data/imagelist.json').success(function(data) {
  //       $scope.imagelist = data;
  //       // console.log($scope.playlist);
  // });

  $scope.addShoper = function () {
      var todo = {
        'event':'create',
        'actions': [],
        'icon': iconsArray[Math.floor(Math.random()*iconsArray.length)],
        'color': '#'+(Math.random()*0xFFFFFF<<0).toString(16),
        'filmname': curtime.getFilmName(),
        'title': (new Date().valueOf()).toString(36),
        'sex': $scope.formData.sex.name,
        'age': $scope.formData.age.name,
        'person': $scope.formData.person.name,
        'child': $scope.formData.child,
        'trolley': $scope.formData.trolley.name,
        'curtime' : curtime.getCurrentTime()
      }
      $scope.todos.push(todo);

      $scope.formData.title = '';
      $scope.formData.sex = '';
      $scope.formData.age = '';
      $scope.formData.person = '';
      $scope.formData.child = '';
      $scope.formData.trolley = '';

      var dataObj = {item: $scope.todos.slice(-1)[0]};
      $http.post('api/add', dataObj)
        .success(function(data, status, headers, config) {
          $scope.massage = data;
        })
        .error(function(data, status, headers, config){
          //console.log(error);
          alert( "failure message: " + JSON.stringify({data: data}));
      });

      if ($scope.todos.length == 1){
        $scope.showAction(0);
      }
  };

  $scope.showAction = function (index) {
    if (angular.isUndefined($scope.todos[index])){
      $scope.actionUser = false;
    } else {
      $scope.selectedIndex = index;
      $scope.actionUserIcon = $scope.todos[index]['icon'];
      $scope.actionUserColor = $scope.todos[index]['color'];
      $scope.inx = index;
      // setTimeout(function(){},100);
      $scope.actionUser = true;
      $scope.selectedAreaName = null;
      $scope.selectedActionIndex = null;
    }
  };

  $scope.AddAction = function(index){
    $scope.selectedActionIndex = index;

    var action = $scope.actions[index];
    var todo = $scope.todos[$scope.inx];

    todo['event'] = action['event'];
    todo['curtime'] = curtime.getCurrentTime();

    if (action['event'] == 'watch' || action['event'] == 'touching' || action['event'] == 'purchase') {
      todo['image'] = $scope.selectedAreaName;
    } else {
      todo['image'] = "";
    }
    todo['actions'].push(action['name'] + ' ' + todo['image']);

    //console.log(todo);
    //console.log(curtime.getCurrentTime());

    var dataObj = {item: $scope.todos[$scope.inx]};
    $http.post('api/add', dataObj)
      .success(function(data, status, headers, config) {
        $scope.massage = data;
      })
      .error(function(data, status, headers, config){
        //console.log(error);
        alert( "failure message: " + JSON.stringify({data: data}));
    });

    if ($scope.actions[index]['event'] == 'exit'){
      $scope.past_todos.unshift(todo);
      $scope.todos.splice($scope.inx, 1);
      $scope.showAction(0);
      $scope.selectedActionIndex = null;
    }
  };

  $scope.imageDetails = function(){
    if ($scope.showImageList){
      $scope.showImageList = false;
      $scope.showImage = false;
    }else{
      $scope.showImageList = true;
      $scope.showImage = true;
    }
  };

  $scope.imageSelected = function(i) {

    //$window.location.reload(true);
    $scope.image_url = $scope.imagelist[i].path;
    $scope.textSource = $scope.imagelist[i].txt;
    $scope.showImageList = false;
    $scope.showImage = false;

    $http.get($scope.textSource)
    .success(function(data) {
      $scope.ImgData = $sce.trustAsHtml(data);
      // addMap();
    })
    .error(function(error){
      console.log(error);
    });

  };

  $scope.image_changed = function(element) {
    //console.log(element.files[0]);
     $scope.$apply(function(scope) {
        $scope.image_url = URL.createObjectURL(element.files[0]);
     });
  };
  $scope.map_changed = function(element) {
    //console.log(element.files[0]);
     $scope.$apply(function(scope) {
        $scope.map_url = URL.createObjectURL(element.files[0]);

     });
  };

  $scope.addImage = function(){
     $http.get($scope.map_url)
          .success(function(data) {
            $scope.ImgData = $sce.trustAsHtml(data);

            $timeout(function () {
              addMap();
            }, 0, false);
          })
          .error(function(error){
            console.log(error);
          })
    $scope.showImageList = false;
    $scope.showImage = false;
  };

  $scope.showContent = function($fileContent){
        $scope.content = $fileContent;
  };

  $scope.callJquery = function (var1) {
    if ($scope.todos[$scope.inx] && $scope.selectedAreaName != var1){
      $scope.selectedAreaName = var1;
      $scope.selectedActionIndex = null;
    }
  };

  $scope.todoDescription = function(todo){
    var items = new Array(todo.sex, todo.age, todo.person, todo.trolley);
    if (todo.child) {
      items.push('z dzieckiem');
    }
    return items.join(", ");
  };

  $scope.isDisabledAction = function(action) {
    var event = action['event'];
    if (event == 'stop' || event == 'exit') {
      return false;
    } else {
      if ($scope.selectedAreaName) {
        return false;
      }
    }
    return true;
  }

  $scope.areaName = function(action){
    var event = action['event'];
    if (event == 'stop' || event == 'exit') {
      return '';
    } else {
      return $scope.selectedAreaName;
    }
  }

  $scope.removeShopper = function(todo, index){
    todo['event'] = 'destroy';
    var dataObj = {item: todo};
    $http.post('api/add', dataObj)
      .success(function(data, status, headers, config) {
        $scope.massage = data;
      })
      .error(function(data, status, headers, config){
        //console.log(error);
        alert( "failure message: " + JSON.stringify({data: data}));
    });
    $scope.past_todos.splice(index, 1);
  }

  $scope.activateShopper = function(todo, index){
    todo['event'] = 'return';
    todo['actions'].push('powrót');
    var dataObj = {item: todo};
    $http.post('api/add', dataObj)
      .success(function(data, status, headers, config) {
        $scope.massage = data;
      })
      .error(function(data, status, headers, config){
        //console.log(error);
        alert( "failure message: " + JSON.stringify({data: data}));
    });
    $scope.todos.unshift(todo);
    $scope.actionUser = true;
    $scope.selectedAreaName = null;
    $scope.selectedActionIndex = null;

    $scope.past_todos.splice(index, 1);
  }

}]);
