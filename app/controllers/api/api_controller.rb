# class ApiController::BaseController < ::ActionController::Metal
#   include ActionView::Layouts
#   include ActionController::Rendering        # enables rendering
#   include ActionController::MimeResponds     # enables serving different content types like :xml or :json
#   include AbstractController::Callbacks      # callbacks for your authentication logic

#   append_view_path "#{Rails.root}/app/views" # you have to specify your views location as well
# end

module Api
  #class ApiController < ::ActionController::Metal
  class ApiController < ApplicationController
    include AbstractController::Rendering
    include ActionView::Rendering
    include ActionController::Rendering

    def create
      attrs = {}
      [:event, :filmname, :title, :gender, :age, :person_count, :child, :trolley, :curtime, :image].each do |e|
      attrs[e] = case e
        when :person_count; item_params[:person]
        when :gender; item_params[:sex]
        else
          item_params[e]
        end
      end
      item = Item.new(attrs)
      item.user = current_user
      status = item.save ? :ok : :bad_request

      render text: '', status: status
    end

    # def process

    # end
  def item_params
    params.require(:item).permit(:sex, :person, :event, :filmname, :title, :gender, :age, :person_count, :child, :trolley, :curtime, :image)
  end


  end
end
