# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'soap'
set :repo_url, 'git@bitbucket.org:piotrdabrowski/video-matching-rails.git'

# Default branch is :master
#set :branch, proc { `git rev-parse --abbrev-ref surveys`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/apps/video-matching'


# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :bundle_binstubs, nil
set :linked_files, %w{config/database.yml config/secrets.yml .env}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system storage public/carousel}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_ruby_version, '2.3.0'

set :bower_target_path, lambda { "#{release_path}/vendor/assets" }

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
