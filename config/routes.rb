Rails.application.routes.draw do
  devise_for :users
  root 'home#home'

  namespace :api do
    post "add"   => "api#create"
  end

end
