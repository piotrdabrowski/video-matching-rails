class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string  :event
      t.string  :filmname
      t.string  :title
      t.string  :gender
      t.string  :age
      t.string  :person_count
      t.boolean :child
      t.string  :trolley
      t.decimal :curtime, precision: 16, scale: 8
      t.text    :image
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
